---
layout: ../../layouts/project.astro
title: CdM-8 Cheat Sheet
client: Self
publishDate: 2021-02-18 14:51:00
img: /assets/cdm8.png
description: |
  I developed a cheatsheat for the CdM-8 Processor designed by Michael Walters and Alex Shafarenko at the University of Hertfordshire
tags:
  - docs
  - dev
  - processor
---

I delevloped and create this cheatsheat for the CdM-8 processor instructions using mdBook. I made it available for other students to use by setting it up on Gitlab and allowed them to contribute. I like to believe that this project helped out some of the students and made it easier for them to learn the assembly language we had to use.